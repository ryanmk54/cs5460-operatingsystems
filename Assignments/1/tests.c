#define _GNU_SOURCE
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>

// function definitions for assign1.c
unsigned long byte_sort(unsigned long arg);
unsigned long nibble_sort(unsigned long arg);
void draw_me(void);
enum format_t {
  OCT = 66, BIN, HEX
};
void convert(enum format_t mode, unsigned long value);

// function definitions
int main(int argc, char* argv[]);
int test_byte_sort(void);
int test_byte_sort_example(void);
int test_nibble_sort(void);
int test_nibble_sort_example(void);
int test_name_list(void);

int main(int argc, char* argv[])
{
  test_byte_sort();
  test_nibble_sort();
  test_name_list();
  draw_me();
  convert(HEX, 0x0123456789);
  convert(HEX, 0xdeadbeef);
  convert(OCT, 125);
  convert(BIN, 5);
  convert(48, 114);
  return 0;
}


int num_byte_sort_tests = 0;
int num_byte_sort_tests_passed = 0;


int test_byte_sort(void)
{
  printf("Testing byte_sort()\n");
  num_byte_sort_tests_passed += test_byte_sort_example();
  num_byte_sort_tests++;
  printf("Passed %d/%d byte_sort tests\n", num_byte_sort_tests_passed, 
         num_byte_sort_tests);
  return num_byte_sort_tests_passed;
}


// byte_sort (0x0403deadbeef0201) returns 0xefdebead04030201
int test_byte_sort_example(void)
{
  unsigned long ret_val = byte_sort(0x0403deadbeef0201);
  if(ret_val == 0xefdebead04030201)
  {
    return 1;
  }
  printf("byte_sort(0x0403deadbeef0201) should return 0xefdebead04030201, but "
         " instead it returns %lx\n", ret_val);
  return 0;
}


int num_nibble_sort_tests_passed = 0;
int num_nibble_sort_tests = 0;


int test_nibble_sort(void)
{
  printf("Testing nibble_sort()\n");
  num_nibble_sort_tests_passed += test_nibble_sort_example();
  num_nibble_sort_tests++;
  printf("Passed %d/%d nibble_sort tests\n", num_nibble_sort_tests_passed, 
         num_nibble_sort_tests);
  return num_nibble_sort_tests_passed;
}


// nibble_sort (0x0403deadbeef0201) returns 0xfeeeddba43210000
int test_nibble_sort_example(void)
{
  unsigned long ret_val = nibble_sort(0x0403deadbeef0201);
  if(ret_val == 0xfeeeddba43210000)
  {
    return 1;
  }
  printf("nibble_sort(0x0403deadbeef0201) should return 0xfeeeddba43210000 but "
         " instead it returns %lx\n", ret_val);
  return 0;
}

struct elt {
  char val;
  struct elt *link;
};
struct elt *name_list(void);


int test_name_list(void)
{
  printf("test_name_list()\n");
  struct elt* first_letter = name_list();
  if(first_letter == NULL)
  {
    printf("FAIL: first_letter should not be null");
    return 0;
  }
  else if(first_letter->val != 'R')
  {
    printf("FAIL: first_letter->val should be 'R', but it is '%c'\n",
           first_letter->val);
    return 0;
  }
  struct elt* second_letter = first_letter->link;
  if(second_letter == NULL)
  {
    printf("FAIL: second_letter should not be null\n");
    return 0;
  }
  else if(second_letter->val != 'y')
  {
    printf("FAIL: second_letter->val should be 'y', but it is '%c'\n", 
           second_letter->val);
    return 0;
  }
  struct elt* third_letter = second_letter->link;
  if(third_letter == NULL)
  {
    printf("FAIL: third_letter should not be null\n");
    return 0;
  }
  else if(third_letter->val != 'a')
  {
    printf("FAIL: third_letter->val should be 'a', but it is '%c'\n", 
           third_letter->val);
    return 0;
  }
  struct elt* fourth_letter = third_letter->link;
  if(fourth_letter == NULL)
  {
    printf("FAIL: fourth_letter should not be null\n");
    return 0;
  }
  else if(fourth_letter->val != 'n')
  {
    printf("FAIL: fourth_letter->val should be 'n', but it is '%c'\n", 
           fourth_letter->val);
    return 0;
  }
  struct elt* fifth_letter = fourth_letter->link;
  if(fifth_letter != NULL)
  {
    printf("FAIL: fifth_letter should be null, because there is no fifth letter\n");
    return 0;
  }
  printf("test_name_list() passed\n");
  return 1;
}
