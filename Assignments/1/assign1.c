#define _GNU_SOURCE
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/syscall.h> 
#include <unistd.h>

/*********************************************************************
 *
 * These C functions use patterns and functionality often found in
 * operating system code. Your job is to implement them. Of course you
 * should write test cases. However, do not hand in your test cases
 * and (especially) do not hand in a main() function since it will
 * interfere with our tester.
 *
 * Additional requirements on all functions you write:
 *
 * - you may not refer to any global variables
 *
 * - you may not call any functions except those specifically
 *   permitted in the specification
 *
 * - your code must compile successfully on CADE lab Linux
 *   machines when using:
 *
 * /usr/bin/gcc -O2 -fmessage-length=0 -pedantic-errors -std=c99 -Werror -Wall -Wextra -Wwrite-strings -Winit-self -Wcast-align -Wcast-qual -Wpointer-arith -Wstrict-aliasing -Wformat=2 -Wmissing-include-dirs -Wno-unused-parameter -Wshadow -Wuninitialized -Wold-style-definition -c assign1.c 
 *
 * NOTE 1: Some of the specifications below are specific to 64-bit
 * machines, such as those found in the CADE lab.  If you choose to
 * develop on 32-bit machines, some quantities (the size of an
 * unsigned long and the size of a pointer) will change. Since we will
 * be grading on 64-bit machines, you must make sure your code works
 * there.
 *
 * NOTE 2: You should not need to include any additional header files,
 * but you may do so if you feel that it helps.
 *
 * HANDIN: submit your finished file, still called assign.c, in Canvas
 *
 *
 *********************************************************************/


/*********************************************************************
 *
 * byte_sort()
 *
 * specification: byte_sort() treats its argument as a sequence of
 * 8 bytes, and returns a new unsigned long integer containing the
 * same bytes, sorted numerically, with the smaller-valued bytes in
 * the lower-order byte positions of the return value
 * 
 * EXAMPLE: byte_sort (0x0403deadbeef0201) returns 0xefdebead04030201
 *
 *********************************************************************/

unsigned long byte_sort (unsigned long arg)
{
  unsigned long sorted = 0;

  // Find the smallest byte 8 times
  for(int search_num = 0; search_num < 8; search_num++)
  {
    // Search the arg for the smallest byte
    unsigned long smallest_byte = 0xff;
    int smallest_byte_pos = 0;
    for(int byte_num = 0; byte_num < 8; byte_num++)
    {
      unsigned long current_byte = (arg >> (8*byte_num)) & 0xff;
#ifdef DEBUG_BYTE_SORT
      printf("current_byte: %lx\n", current_byte);
#endif
      if(current_byte < smallest_byte)
      {
        smallest_byte = current_byte;
        smallest_byte_pos = byte_num;
      }
    }
#ifdef DEBUG_BYTE_SORT
    printf("smallest byte pos #%d\n", smallest_byte_pos);
#endif

    // We have found the smallest byte
    // Insert it into sorted, by shifting the smallest byte to it's position
    sorted |= smallest_byte << (search_num*8);

    // Set the corresponding position in arg
    // to 0xFF so it will never be the smallest value again
    int bitshift_amount = smallest_byte_pos*8;
    unsigned long bitmask = 0xFF;
      // bitmask needs to be held in a 64-bit variable 
      // so it will shift correctly
    unsigned long reset_val = bitmask << bitshift_amount;
    arg |= reset_val;
#ifdef DEBUG_BYTE_SORT
    printf("arg: %lx\n", arg);
#endif
  }

  return sorted;
}

/*********************************************************************
 *
 * nibble_sort()
 *
 * specification: nibble_sort() treats its argument as a sequence of 16 4-bit
 * numbers, and returns a new unsigned long integer containing the same nibbles,
 * sorted numerically, with smaller-valued nibbles towards the "small end" of
 * the unsigned long value that you return
 *
 * the fact that nibbles and hex digits correspond should make it easy to
 * verify that your code is working correctly
 * 
 * EXAMPLE: nibble_sort (0x0403deadbeef0201) returns 0xfeeeddba43210000
 *
 *********************************************************************/

unsigned long nibble_sort (unsigned long arg)
{
  unsigned long sorted = 0;

  // Find the smallest nibble 16 times
  for(int search_num = 0; search_num < 16; search_num++)
  {
    // Search the arg for the smallest byte
    unsigned long smallest_nibble = 0xf;
    int smallest_nibble_pos = 0;
    for(int nibble_num = 0; nibble_num < 16; nibble_num++)
    {
      unsigned long current_nibble = (arg >> (4*nibble_num)) & 0xf;
#ifdef DEBUG_BYTE_SORT
      printf("current_nibble: %lx\n", current_nibble);
#endif
      if(current_nibble < smallest_nibble)
      {
        smallest_nibble = current_nibble;
        smallest_nibble_pos = nibble_num;
      }
    }
#ifdef DEBUG_BYTE_SORT
    printf("smallest byte pos #%d\n", smallest_nibble_pos);
#endif

    // We have found the smallest byte
    // Insert it into sorted, by shifting the smallest byte to it's position
    sorted |= smallest_nibble << (search_num*4);

    // Set the corresponding position in arg
    // to 0xf so it will never be the smallest value again
    int bitshift_amount = smallest_nibble_pos*4;
    unsigned long bitmask = 0xf;
      // bitmask needs to be held in a 64-bit variable 
      // so it will shift correctly
    unsigned long reset_val = bitmask << bitshift_amount;
    arg |= reset_val;
#ifdef DEBUG_BYTE_SORT
    printf("arg: %lx\n", arg);
#endif
  }

  return sorted;
}

/*********************************************************************
 *
 * name_list()
 *
 * specification: allocate and return a pointer to a linked list of
 * struct elts
 *
 * - the first element in the list should contain in its "val" field the first
 *   letter of your first name; the second element the second letter, etc.;
 *
 * - the last element of the linked list should contain in its "val" field
 *   the last letter of your first name and its "link" field should be a null
 *   pointer
 *
 * - each element must be dynamically allocated using a malloc() call
 *
 * - if any call to malloc() fails, your function must return NULL and must also
 *   free any heap memory that has been allocated so far; that is, it must not
 *   leak memory when allocation fails
 *
 *********************************************************************/

struct elt {
  char val;
  struct elt *link;
};

struct elt *name_list (void)
{
  // Name is Ryan
  // Create memory for each letter
  struct elt* r_elt = malloc(sizeof(struct elt));
  if(r_elt == NULL)
  {
    free(r_elt);
    return(NULL);
  }
  struct elt* y_elt = malloc(sizeof(struct elt));
  if(y_elt == NULL)
  {
    free(r_elt);
    free(y_elt);
    return(NULL);
  }
  struct elt* a_elt = malloc(sizeof(struct elt));
  if(a_elt == NULL)
  {
    free(r_elt);
    free(y_elt);
    free(a_elt);
    return(NULL);
  }
  struct elt* n_elt = malloc(sizeof(struct elt));
  if(n_elt == NULL)
  {
    free(r_elt);
    free(y_elt);
    free(a_elt);
    free(n_elt);
    return NULL;
  }

  // Fill the letter values
  r_elt->val = 'R';
  y_elt->val = 'y';
  a_elt->val = 'a';
  n_elt->val = 'n';

  // Link the letters together
  r_elt->link = y_elt;
  y_elt->link = a_elt;
  a_elt->link = n_elt;
  n_elt->link = NULL;
  return r_elt;
}

/*********************************************************************
 *
 * convert()
 *
 * specification: depending on the value of "mode", print "value" as
 * octal, binary, or hexidecimal to stdout, followed by a newline
 * character
 *
 * extra requirement 1: output must be via putc()
 *
 * extra requirement 2: print nothing if "mode" is not one of OCT,
 * BIN, or HEX
 *
 * extra requirement 3: all leading/trailing zeros should be printed
 *
 * EXAMPLE: convert (HEX, 0xdeadbeef) should print
 * "00000000deadbeef\n" (including the newline character but not
 * including the quotes)
 *
 *********************************************************************/

enum format_t {
  OCT = 66, BIN, HEX
};

void convert (enum format_t mode, unsigned long value)
{
  int num_bits;
  int num_vals;
  if(mode == OCT)
  {
    num_bits = 2;
    num_vals = 8;
  }
  else if(mode == BIN)
  {
    num_bits = 1;
    num_vals = 2;
  }
  else if(mode == HEX)
  {
    num_bits = 4;
    num_vals = 16;
  }
  else
  {
    return;
  }
  int num_digits = 64/num_bits;
  char last_char[num_digits];

  for(int digit = 0; digit < num_digits; digit++)
  {
    int mod = value % num_vals;
    value = value / num_vals;
    char char_to_print;
    if(mod < 10)
    {
      char_to_print = mod + 48;
    }
    else
    {
      char_to_print = mod - 10 + 97;
    }

    last_char[digit] = char_to_print;
  }
  for(int digit = num_digits - 1; digit >= 0; digit--)
  {
    putc(last_char[digit], stdout);
  }
  putc('\n', stdout);
}

/*********************************************************************
 *
 * draw_me()
 *
 * this function creates a file called me.txt which contains an ASCII-art
 * picture of you (it does not need to be very big). the file must (pointlessly,
 * since it does not contain executable content) be marked as executable.
 * 
 * extra requirement 1: you may only call the function syscall() (type "man
 * syscall" to see what this does)
 *
 * extra requirement 2: you must ensure that every system call succeeds; if any
 * fails, you must clean up the system state (closing any open files, deleting
 * any files created in the file system, etc.) such that no trash is left
 * sitting around
 *
 * you might be wondering how to learn what system calls to use and what
 * arguments they expect. one way is to look at a web page like this one:
 * http://blog.rchapman.org/post/36801038863/linux-system-call-table-for-x86-64
 * another thing you can do is write some C code that uses standard I/O
 * functions to draw the picture and mark it as executable, compile the program
 * statically (e.g. "gcc foo.c -O -static -o foo"), and then disassemble it
 * ("objdump -d foo") and look at how the system calls are invoked, then do
 * the same thing manually using syscall()
 *
 *********************************************************************/

char* strerror(int);
void draw_me (void)
{
    int fp = syscall(SYS_open, "me.txt", 
                     O_CREAT | O_RDWR, S_IRWXU | S_IRWXG | S_IRWXO);
    const char* buf_to_write = "   ******* \n"
                               "  |  *  * |\n"
                               "  |    >  |\n"
                               "   \\_'-'_/\n"
                               " ___|  |___\n"
                               "/          \\\n"
                               "| |       | |\n"
                               "| |       | |\n"
                               "| |       | |\n"
                               "\\/_______ \\/\n"
                               "|           |\n"
                               "|           |\n"
                               "|     |     |\n"
                               "|     |     |\n"
                               "|     |     |\n"
                               "\\     \\     \\\n"
                               " \\----//-----//\n";
    if(syscall(SYS_write, fp, buf_to_write, 227) <= 0)
        syscall(SYS_unlink, "me.txt");
    syscall(SYS_close, fp);
}

