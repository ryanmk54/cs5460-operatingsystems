#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/fcntl.h>


void print_usage() {
    printf("Usage:  ./c-write bytes file\n");
    printf("writes the bytes given by bytes to the file\n");
}


/**
 * argv[1] bytes to write
 * argv[2] file to write to
 */
int main(int argc, char* argv[]) {
    if (3 != argc) {
        print_usage();
        return -1;
    }

    char* bytes = argv[1];
    int num_bytes = strlen(bytes);
    char* path = argv[2];

    int fd = open(path, O_WRONLY);
    if (fd < 0) {
    	perror("Unable to open file");
	exit(-1);
    }
    size_t num_bytes_written = write(fd, bytes, num_bytes);
    if (close(fd)) {
    	perror("Unable to close file");
	exit(-1);
    }
    printf("%zu bytes written\n", num_bytes_written);
    fflush(stdout);
    exit(num_bytes_written);
}
