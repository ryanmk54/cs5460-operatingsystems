#include <assert.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


/* Forward Declarations */
void print_usage(void);
void play(void);
void cat_enter(void);
void cat_exit(void);
void dog_enter(void);
void dog_exit(void);
void bird_enter(void);
void bird_exit(void);
void* cat_play(void*);
void* dog_play(void*);
void* bird_play(void*);

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
    /* a single mutex */
volatile int time_to_play = 1;
    /* set to zero when the threads should stop work */

/* condition variable for each kind of animal */
pthread_cond_t cat_cond = PTHREAD_COND_INITIALIZER;
pthread_cond_t dog_cond = PTHREAD_COND_INITIALIZER;
pthread_cond_t bird_cond = PTHREAD_COND_INITIALIZER;


/* Number of each animal on the playground */
volatile int cats = 0;
volatile int dogs = 0;
volatile int birds = 0;

/* Total number of each animal */
volatile int n_cats;
volatile int n_dogs;
volatile int n_birds;

/* number of times each animal entered the playground */
volatile int n_cats_entered = 0;
volatile int n_dogs_entered = 0;
volatile int n_birds_entered = 0;


/**
 * Takes 3 args:  # cats, # dogs, #birds
 * Runs for 10 seconds
 */
int main(int argc, char* argv[]) {
    if (4 != argc) {
        printf("Wrong number of arguments\n");
        print_usage();
        return -1;
    }

    /* Make sure the total number of animals are in range */
    n_cats = atoi(argv[1]);
    n_dogs = atoi(argv[2]);
    n_birds = atoi(argv[3]);
    if (n_cats < 0 || n_cats > 99 || 
            n_dogs < 0 || n_dogs > 99 ||
            n_birds < 0 || n_birds > 99) {
        print_usage();
        return -1;
    }

    /* Animals represented by threads */
    pthread_t cat_threads[n_cats];
    pthread_t dog_threads[n_dogs];
    pthread_t bird_threads[n_birds];

    /* Initialize the animal threads and let them fight for the playground */
    for (int i=0; i<n_cats; i++) {
        if (pthread_create(&cat_threads[i], NULL, cat_play, NULL)){
            perror("Unable to create a thread");
        }
    }
    for (int i=0; i<n_dogs; i++) {
        if (pthread_create(&dog_threads[i], NULL, dog_play, NULL)){
            perror("Unable to create a thread");
        }
    }
    for (int i=0; i<n_birds; i++) {
        if (pthread_create(&bird_threads[i], NULL, bird_play, NULL)){
            perror("Unable to create a thread");
        }
    }

    /* After 10 seconds, join the threads, and print out 
     * the number of cats, dogs, and birds that were able to play */
    sleep(10);
    time_to_play = 0;
    for (int i=0; i<n_cats; i++) {
        if (pthread_join(cat_threads[i], NULL)){
            perror("Unable to join a thread");
        }
    }
    for (int i=0; i<n_dogs; i++) {
        if (pthread_join(dog_threads[i], NULL)){
            perror("Unable to join a thread");
        }
    }
    for (int i=0; i<n_birds; i++) {
        if (pthread_join(bird_threads[i], NULL)){
            perror("Unable to join a thread");
        }
    }
    printf("cat play = %d dog play = %d bird play = %d\n", n_cats_entered, 
            n_dogs_entered, n_birds_entered);
}


void print_usage(void) {
    printf("Usage: problem_1 num_cats num_dogs num_birds\n");
    printf("num_cats, num_dogs, and num_birds all need to be \n");
    printf("between 0 and 99 inclusive\n");
}


void cat_enter(void) {
    pthread_mutex_lock(&lock);

    /* cats can't play with dogs or birds */
    while (dogs || birds) {
        pthread_cond_wait(&cat_cond, &lock);
    }

    cats++;
    pthread_mutex_unlock(&lock);
}


void cat_exit(void) {
    pthread_mutex_lock(&lock);
    cats--;

    /* if all the cats are gone, birds and dogs can play */
    if (0==cats){
        pthread_cond_broadcast(&bird_cond);
        pthread_cond_broadcast(&dog_cond);
    }

    pthread_mutex_unlock(&lock);
}


void dog_enter(void) {
    pthread_mutex_lock(&lock);

    /* dogs can't play with cats */
    while (cats) {
        pthread_cond_wait(&dog_cond, &lock);
    }

    dogs++;
    pthread_mutex_unlock(&lock);
}


void dog_exit(void) {
    pthread_mutex_lock(&lock);
    dogs--;

    if (0==dogs) {
        pthread_cond_broadcast(&cat_cond);
    }

    pthread_mutex_unlock(&lock);
} 


void bird_enter(void) {
    pthread_mutex_lock(&lock);

    /* birds can't enter at the same time as cats */
    while (cats) {
        pthread_cond_wait(&bird_cond, &lock);
    }

    birds++;
    pthread_mutex_unlock(&lock);
}


void bird_exit(void) {
    pthread_mutex_lock(&lock);
    birds--;

    /* if the birds are gone, dogs and cats can race 
     * to the playground.  Only one of them will be let in */
    if (0==birds) {
        pthread_cond_broadcast(&dog_cond);
        pthread_cond_broadcast(&cat_cond);
    }

    pthread_mutex_unlock(&lock);
}


void* cat_play(void* arg) {
    while (time_to_play) {
        cat_enter();
        n_cats_entered++;
        play();
        cat_exit();
    }
    return NULL;
}


void* dog_play(void* arg) {
    while (time_to_play) {
        dog_enter();
        n_dogs_entered++;
        play();
        dog_exit();
    }
    return NULL;
}


void* bird_play(void* arg) {
    while (time_to_play) {
        bird_enter();
        n_birds_entered++;
        play();
        bird_exit();
    }
    return NULL;
}


/* play code copied from the instructions */
void play(void) {
    for (int i=0; i<10; i++) {
        assert(cats >= 0 && cats <= n_cats);
        assert(dogs >= 0 && dogs <= n_dogs);
        assert(birds >= 0 && birds <= n_birds);
        assert(cats == 0  || dogs == 0);
        assert(cats == 0 || birds == 0);
    }
}
