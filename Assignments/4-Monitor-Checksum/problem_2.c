#include "crc32.h"

#include <dirent.h>
#include <fcntl.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>


/* Forward method declarations */
int calc_checksum_of_file(const char* file_path, uint32_t* checksum_ptr);
int count_files_in_dir(char* directory_name);
int cstr_cmp(const void* a, const void* b);
char* dir_plus_filename(const char* dir_path, const char* filename);
char* dir_plus_slash(const char* dir_path);
int ends_in_slash(const char* str);
char** get_file_names_in_dir(char* directory_name, int n_files);
void print_usage(void);
int should_skip_file(unsigned char d_type);
void try_close(int fd);
void* try_malloc(size_t size);
DIR* try_opendir(char* dir_name);

int main(int argc, char* argv[]) {
    if (argc != 2) {
        print_usage();
        return -1;
    }

    char* directory_name = dir_plus_slash(argv[1]);
    int n_files = count_files_in_dir(directory_name);
    char** filenames = get_file_names_in_dir(directory_name, n_files);

    /* Instr:  
     * By default, readdir won't give files in a particular order
     * print them in sorted order by running strcmp() on the file names */
    qsort(filenames, n_files, sizeof(char*), cstr_cmp);

    /* Calculate and print the checksum for each file */
    for (int i = 0; i < n_files; i++) {
        const char* file_path = dir_plus_filename(directory_name, filenames[i]);

        /* Instr:  
         * The checksum should be printed in uppercase hex 
         * including any leading zeroes necessary to pad out 
         * to 8 hex digits
         *
         * Instr:  
         * The file name and checksum should be separated 
         * by a space and followed by a newline */
        uint32_t* checksum = malloc(sizeof(uint32_t));
        if (calc_checksum_of_file(file_path, checksum)) { 
            printf("%s %s\n", filenames[i], "ACCESS ERROR");
        }
        else {
            printf("%s %08X\n", filenames[i], *checksum);
        }
    }
}

/**
 * Calculates the crc32 checksum of the file at the given path
 */
int calc_checksum_of_file(const char* file_path, uint32_t* checksum_ptr) {
    int BUF_SIZE = 2048;
    int read_size = 0;
    char* file_buf = malloc(BUF_SIZE);

    int file = open(file_path, O_RDONLY);
    if (-1 == file) {
        return -1;
    }

    /* calculate the checksum */
    uint32_t checksum = 0;
    do {
        read_size = read(file, file_buf, BUF_SIZE);
        if (-1 == read_size) {
            return -1;
        }
        checksum = crc32(checksum, file_buf, read_size);
    } while (read_size != 0);

    try_close(file);

    *checksum_ptr = checksum;
    return 0;
}


/**
 * Tries to close a file
 * Options can be found in the command 'man 2 close'
 * Exits on error
 */
void try_close(int fd) {
    int status = close(fd);
    if (status) {
        perror("Unable to close file");
        exit(-1);
    }
}


/**
 * returns 1 if the given string ends in a '/' character
 */
int ends_in_slash(const char* str) {
    int len = strlen(str);
    char end_letter = str[len - 1];
    return '/' == end_letter;
}

/** 
 * Combines the path of a directory with a filename 
 */
char* dir_plus_filename(const char* dir_path, const char* filename) {
    char* path = try_malloc(sizeof(char) * 
            (strlen(dir_path) + strlen(filename) + 1));
    strcpy(path, dir_path);
    if (!ends_in_slash(dir_path)) {
        strcat(path, "/");
    }
    strcat(path, filename);
    return path;
}


/**
 * Adds a slash to the ends of the directory if it needs it
 */
char* dir_plus_slash(const char* dir_path) {
    char* path = try_malloc(sizeof(char) * (strlen(dir_path) + 1));
    strcpy(path, dir_path);
    if (!ends_in_slash(dir_path)) {
        strcat(path, "/");
    }
    return path;
}




/**
 * Returns the number of filenames in a directory
 */
int count_files_in_dir(char* directory_name) {
    int n_files = 0;

    char* dir_path = dir_plus_slash(directory_name);
    DIR* directory = try_opendir(dir_path);
    for (struct dirent* dir_entry = readdir(directory);
            dir_entry != NULL; dir_entry = readdir(directory)) {

        if (should_skip_file(dir_entry->d_type)) {
            continue;
        }

        n_files++;
    }
    return n_files;
}


/**
 * Returns an an unsorted array of all the filenames in a directory
 */
char** get_file_names_in_dir(char* directory_name, int n_files) {
    char** file_names = try_malloc(sizeof(char*) * n_files);

    int file_num = 0;
    char* dir_path = dir_plus_slash(directory_name);
    DIR* dir = try_opendir(dir_path);
    for (struct dirent* dir_entry = readdir(dir);
            dir_entry != NULL; dir_entry = readdir(dir)) {

        /* get file name */
        char* file_name = dir_entry->d_name;

        if (should_skip_file(dir_entry->d_type)) {
            continue;
        } 

        file_names[file_num] = file_name;
        file_num++;
    }
    return file_names;
}


/**
 * Skip all files that aren't regular files
 */
int should_skip_file(unsigned char d_type) {
    if (DT_REG != d_type){
        return 1;
    }
    return 0;
}


/**
 * Calls opendir with the given dir_name.
 * Exits on failure
 */
DIR* try_opendir(char* dir_name) {

    /* Instr:  
     * If the directory on the command line doesn't exist or can't be read
     * exit with a friendly error message */
    DIR* dir = opendir(dir_name);
    if (NULL == dir) {
        perror("Unable to open directory");
        exit(-1);
    }
    return dir;
}


/**
 * Calls malloc
 * Exits on failure
 */
void* try_malloc(size_t size) {
    void* mem_ptr = malloc(size);
    if (NULL == mem_ptr && 0 != size) {
        printf("Unable to malloc\n");
        exit(-1);
    }
    return mem_ptr;
}



void print_usage(void) {
    printf("Usage:  problem_2 name_of_directory\n");
}

/* taken from:
 * http://stackoverflow.com/questions/13249756/explicit-ignore-warning-from-wcast-qual-cast-discards-attribute-const
 */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"

/*taken from:  
 *http://anyexample.com/programming/c/qsort__sorting_array_of_strings__integers_and_structs.xml
 */
int cstr_cmp(const void* a, const void* b) {
    const char** a_str = (const char**)a;
    const char** b_str = (const char**)b;
    return strcmp(*a_str, *b_str);
}
#pragma GCC diagnostic pop
