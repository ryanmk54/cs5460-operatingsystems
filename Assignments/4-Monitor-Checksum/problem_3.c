#include "crc32.h"

#include <dirent.h>
#include <fcntl.h>
#include <inttypes.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>


/* Forward method declarations */
int calc_checksum_of_file(const char* file_path, uint32_t* checksum_ptr);
int count_files_in_dir(char* directory_name);
int cstr_cmp(const void* a, const void* b);
char* dir_plus_filename(const char* dir_path, const char* filename);
char* dir_plus_slash(const char* dir_path);
int ends_in_slash(const char* str);
char** get_file_names_in_dir(char* directory_name, int n_files);
void print_usage(void);
int should_skip_file(unsigned char d_type);
void* thread_start(void* arg);
void try_close(int fd);
void* try_malloc(size_t size);
DIR* try_opendir(char* dir_name);

/* global variables */
char* dir_path = NULL;
volatile int* checksums = NULL;
volatile int* calculating = NULL;
int n_files = 0;
char** filenames = NULL;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

int main(int argc, char* argv[]) {
    if (argc != 3) {
        print_usage();
        return -1;
    }

    dir_path = dir_plus_slash(argv[1]);
    int num_threads = atoi(argv[2]);
    n_files = count_files_in_dir(dir_path);
    filenames = get_file_names_in_dir(dir_path, n_files);

    /* Instr:  
     * By default, readdir won't give files in a particular order
     * print them in sorted order by running strcmp() on the file names */
    qsort(filenames, n_files, sizeof(char*), cstr_cmp);

    /* Create threads */
    /* TODO compute the checksums in a thread 
     * the output must be the same as problem_2*/
    checksums = malloc(sizeof(int*) * n_files);
    calculating  = malloc(sizeof(int*) * n_files);
    pthread_t* threads = malloc(sizeof(pthread_t*) * num_threads);
    for (int i = 0; i < num_threads; i++) {
        pthread_create(&threads[i], NULL, thread_start, NULL);
    }

    /* Join the threads */
    for (int i = 0; i < num_threads; i++) {
        pthread_join(threads[i], NULL);
    }

    /* Strategy:  
     * create a checksums array
     * create a calcuating array
     * in the threads method, set working before calucating checksum
     * each thread checks the array in order to get the first
     *   available file to checksum
     */

    /* Calculate and print the checksum for each file */
    for (int i = 0; i < n_files; i++) {
        /* Instr:  
         * The checksum should be printed in uppercase hex 
         * including any leading zeroes necessary to pad out 
         * to 8 hex digits
         *
         * Instr:  
         * The file name and checksum should be separated 
         * by a space and followed by a newline */
        if (calculating[i] == 1) {
            printf("%s %08X\n", filenames[i], checksums[i]);
        }
        else if (calculating[i] == 2) {
            printf("%s ACCESS ERROR\n", filenames[i]);
        }
    }
}

void* thread_start(void* arg) {
    while (1) {
        pthread_mutex_lock(&lock);
        int pos = -1;

        /* look through all the file_names 
         * and find one that hasn't had the checksum calculated */
        for (int i = 0; i < n_files; i++) {
            if (!calculating[i]) {
                calculating[i] = 1;
                pos = i;
                break;
            }
        }
        pthread_mutex_unlock(&lock);

        /* if there was no position to calcuate, we are all done */
        if (-1 == pos) {
            pthread_exit(NULL);
        }

        const char* file_path = dir_plus_filename(dir_path, filenames[pos]);
        uint32_t* checksum = malloc(sizeof(uint32_t));
        if (calc_checksum_of_file(file_path, checksum)) { 
            calculating[pos] = 2;
        }
        else {
            checksums[pos] = *checksum;
        }
    }
    return NULL;
}

/**
 * Calculates the crc32 checksum of the file at the given path
 */
int calc_checksum_of_file(const char* file_path, uint32_t* checksum_ptr) {
    int BUF_SIZE = 2048;
    int read_size = 0;
    char* file_buf = malloc(BUF_SIZE);

    int file = open(file_path, O_RDONLY);
    if (-1 == file) {
        return -1;
    }

    /* calculate the checksum */
    uint32_t checksum = 0;
    do {
        read_size = read(file, file_buf, BUF_SIZE);
        if (-1 == read_size) {
            return -1;
        }
        checksum = crc32(checksum, file_buf, read_size);
    } while (read_size != 0);

    try_close(file);

    *checksum_ptr = checksum;
    return 0;
}


/**
 * Tries to open the file at the given file path read only.
 * Exits on failure
 */
int try_open_readonly(const char* file_path) {
    int fd = open(file_path, O_RDONLY);

    /* Check to see if we were able to open the file */
    if (-1 == fd) {
        perror("Unable to open file for reading");
        exit(-1);
    }
    return fd;
}


/**
 * Tries to read a file.  
 * Options can be found in the command 'man 2 read'
 * Exits on error
 */
int try_read(int fd, void* buf, size_t count) {
    ssize_t num_bytes_read = read(fd, buf, count);
    if (-1 == num_bytes_read) {
        perror("Unable to read file");
        exit(-1);
    }
    return num_bytes_read;
}


/**
 * Tries to close a file
 * Options can be found in the command 'man 2 close'
 * Exits on error
 */
void try_close(int fd) {
    int status = close(fd);
    if (status) {
        perror("Unable to close file");
        exit(-1);
    }
}


/**
 * returns 1 if the given string ends in a '/' character
 */
int ends_in_slash(const char* str) {
    int len = strlen(str);
    char end_letter = str[len - 1];
    return '/' == end_letter;
}

/** 
 * Combines the path of a directory with a filename 
 */
char* dir_plus_filename(const char* directory_path, const char* filename) {
    char* path = try_malloc(sizeof(char) * 
            (strlen(directory_path) + strlen(filename) + 1));
    strcpy(path, directory_path);
    if (!ends_in_slash(directory_path)) {
        strcat(path, "/");
    }
    strcat(path, filename);
    return path;
}



/**
 * Returns the number of filenames in a directory
 */
int count_files_in_dir(char* directory_name) {
    int num_files = 0;

    DIR* directory = try_opendir(directory_name);
    for (struct dirent* dir_entry = readdir(directory);
            dir_entry != NULL; dir_entry = readdir(directory)) {
        
        if (should_skip_file(dir_entry->d_type)) {
            continue;
        }

        num_files++;
    }
    return num_files;
}


/**
 * Returns an an unsorted array of all the filenames in a directory
 */
char** get_file_names_in_dir(char* directory_name, int num_files) {
    char** file_names = try_malloc(sizeof(char*) * num_files);

    int file_num = 0;
    DIR* dir = try_opendir(directory_name);
    for (struct dirent* dir_entry = readdir(dir);
            dir_entry != NULL; dir_entry = readdir(dir)) {

        /* get file name */
        char* file_name = dir_entry->d_name;

        if (should_skip_file(dir_entry->d_type)) {
            continue;
        }

        file_names[file_num] = file_name;
        file_num++;
    }
    return file_names;
}


/**
 * Skip all files that aren't regular files
 */
int should_skip_file(unsigned char d_type) {
    if (DT_REG != d_type){
        return 1;
    }
    return 0;
}


/**
 * Calls opendir with the given dir_name.
 * Exits on failure
 */
DIR* try_opendir(char* dir_name) {

    /* Instr:  
     * If the directory on the command line doesn't exist or can't be read
     * exit with a friendly error message */
    DIR* dir = opendir(dir_name);
    if (NULL == dir) {
        perror("Unable to open directory");
        exit(-1);
    }
    return dir;
}


/**
 * Calls malloc
 * Exits on failure
 */
void* try_malloc(size_t size) {
    void* mem_ptr = malloc(size);
    if (NULL == mem_ptr && 0 != size) {
        printf("Unable to malloc\n");
        exit(-1);
    }
    return mem_ptr;
}



/**
 * Adds a slash to the ends of the directory if it needs it
 */
char* dir_plus_slash(const char* directory_path) {
    char* path = try_malloc(sizeof(char) * (strlen(directory_path) + 1));
    strcpy(path, directory_path);
    if (!ends_in_slash(directory_path)) {
        strcat(path, "/");
    }
    return path;
}


void print_usage(void) {
    printf("Usage:  problem_3 name_of_directory number_of_threads\n");
}

/* pragma taken from:
 * http://stackoverflow.com/questions/13249756/explicit-ignore-warning-from-wcast-qual-cast-discards-attribute-const
 */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"

/* c string compare with qsort taken from:  
 *http://anyexample.com/programming/c/qsort__sorting_array_of_strings__integers_and_structs.xml
 */
int cstr_cmp(const void* a, const void* b) {
    const char** a_str = (const char**)a;
    const char** b_str = (const char**)b;
    return strcmp(*a_str, *b_str);
}
#pragma GCC diagnostic pop
