#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

#define MAX_THREADS 100
#define TRUE 1
#define FALSE 0
#define handle_error_en(en, msg) \
       do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

/* Types */
typedef struct point{
    double x;
    double y;
} point_t;

/* Forward declarations fo functions */
double gen_random_num_between_0_and_1();
point_t gen_random_point();
double gen_random_val_in_square();
int is_point_in_circle(point_t point);
int main(int argc, char* argv[]);
void* thread_start();

/* Global Variables */
char* usage_statement = ""
"Usage ./problem_6 threads seconds\n"
"A program that calculates pi using using the Monte Carlo method.\n";
int time_to_stop = FALSE;
volatile uint64_t num_points_in_circle = 0;
volatile uint64_t num_points = 0;
pthread_mutex_t* global_mutex;


/**
 * A program that calculates pi using the Monte Carlo method.
 */
int main(int argc, char* argv[]){
    /* Verify we have the correct arguments */
    if(argc != 3){
        printf("%s", usage_statement);
        return 1;
    }

    /* Parse the arguments */
    int num_threads = atoi(argv[1]);
    int num_seconds = atoi(argv[2]);

    /* Validate the input arguments */
    if(num_threads < 1 || num_seconds < 1){
        printf("Arguments must be at least 1\n");
        return 1;
    }

    /* Initialize the mutex lock */
    global_mutex = malloc(sizeof(pthread_mutex_t));
    pthread_mutex_init(global_mutex, NULL);

    /* Create threads and have them start calculating */
    pthread_t thread_ids[num_threads];
    int thread_num;
    for(thread_num = 0; thread_num < num_threads; thread_num++){
        /* Create the thread */
        pthread_attr_t thread_attr;
        pthread_attr_init(&thread_attr);
        int ret_val = pthread_create(&thread_ids[thread_num], &thread_attr, 
                *thread_start, NULL);

        /* Verify we were able to create a thread */
        if(ret_val != 0){
            handle_error_en(ret_val, "Error creating thread:");
        }
    }

    /* sleep for some number of seconds to allow the other threads to do 
     * calculations */
    sleep(num_seconds);
    time_to_stop = TRUE;
    for(thread_num = 0; thread_num < num_threads; thread_num++){
        void* status = malloc(sizeof(void*));
        pthread_join(thread_ids[thread_num], &status);

        /* Verify we were able to join the thread */
        if(status != 0){
            printf("Error joining a thread %lu\n", (long)status);
            exit(2);
        }
    }
    long double pi = (double)(4 * num_points_in_circle) / num_points;
    printf("pi is estimated to be %Lf\n", pi);
    return 0;
}


/**
 * Generates a random point.
 * Updates the number of points in the circle and the number of points.
 */
void* thread_start(){
    while(!time_to_stop){
        point_t point = gen_random_point();
        pthread_mutex_lock(global_mutex);
        if(is_point_in_circle(point))
            num_points_in_circle++;
        num_points++;
        pthread_mutex_unlock(global_mutex);
    }
    return NULL;
}


/**
 * Generates a random x,y point where x and y are between -1 and +1
 */
point_t gen_random_point(){
    point_t random_point;
    random_point.x = gen_random_val_in_square();
    random_point.y = gen_random_val_in_square();
    return random_point;
}


/**
 * Generates a random point between 0 and 1
 */
double gen_random_num_between_0_and_1(){
    return (double)(rand())/RAND_MAX;
}


/**
 * Generates a random value within the bounds of the square
 */
double gen_random_val_in_square(){
    return 2*gen_random_num_between_0_and_1() - 1;
}



/**
 * Checks whether the specified point is inside the circle.
 */
int is_point_in_circle(point_t point){
    if(point.x*point.x + point.y*point.y <= 1)
        return TRUE;
    else
        return FALSE;
}
