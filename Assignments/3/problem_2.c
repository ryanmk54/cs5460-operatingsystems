#include <assert.h>
#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX_THREADS 100
#define TRUE 1
#define FALSE 0
#define handle_error_en(en, msg) \
       do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

/* Forward declarations fo functions */
int main(int argc, char* argv[]);
void critical_section();
void lock(int i);
int max_num();
int max_val_in_arr(int arr[], int arr_size);
void* thread_start(void* arg);
void unlock(int i);

/* Global Variables */
volatile int in_cs = 0;
volatile int empty_spot_in_arr = 0;
int num_threads = 0;
volatile int time_to_finish;
pthread_t threads[MAX_THREADS];
int num_times_entered_critical_section[100];
volatile int entering[100];
volatile int number[100];

char* usage_statement = ""
"Usage ./problem_2 threads seconds\n"
"A pthreads program that creates a given number of threads\n"
"that repeatedly access a critical section for a given number\n"
"seconds.  The threads are synchronized using Lamport's Bakery "
"Algorithm.\n";


/**
 * A pthreads program that creates a number of threads that repeatedly access 
 * a critical section.  The threads are synchronized using Lamport's Bakery
 * Algorithm.
 */
int main(int argc, char* argv[]){
    /* Verify we have the correct arguments */
    if(argc != 3){
        printf("%s", usage_statement);
        return 1;
    }

    /* Parse the arguments */
    num_threads = atoi(argv[1]);
    int n_seconds = atoi(argv[2]);

    if(num_threads < 1 || n_seconds < 1){
        printf("Arguments must be at least 1\n");
        return 1;
    }

    /* Create the threads */
    time_to_finish = 0;
    int thread_num;
    for(thread_num = 0; thread_num < num_threads; thread_num++){
        /* Get an id for this thread */
        int* pos_in_arr = malloc(sizeof(int));
        *pos_in_arr = empty_spot_in_arr++;

        /* Create the thread */
        pthread_attr_t thread_attr;
        pthread_attr_init(&thread_attr);
        int ret_val = pthread_create(&threads[thread_num], &thread_attr, *thread_start, pos_in_arr);

        /* Verify we were able to create a thread */
        if(ret_val != 0){
            handle_error_en(ret_val, "Error creating thread:");
        }
    }

    /* sleep for some number of seconds to allow the threads to enter and exit
     * the critical section */
    sleep(n_seconds);

    /* Signal to the threads that it is time to stop working */
    time_to_finish = 1;

    /* Wait here until the threads are done */
    for(thread_num = 0; thread_num < num_threads; thread_num++){
        void* status = malloc(sizeof(void*));
        pthread_join(threads[thread_num], &status);

        /* Verify we were able to join the thread */
        if(status != 0){
            printf("Error joining a thread %lu\n", (long)status);
            exit(2);
        }
    }

    return 0;
}


/**
 * locks, cals the critical section, then unlocks.
 * Continues until the global time_to_finish flag is set.
 */
void* thread_start(void* arg){
    int pos_in_arr = *((int*)arg);

    /* count the number of times the critical section is entered */
    int num_times_entered_critical_section = 0;

    /* Lock, enter the critical section, unlock */
    while(!time_to_finish){
        lock(pos_in_arr);
        num_times_entered_critical_section += 1;
        critical_section();
        unlock(pos_in_arr);
    }

    /* print how many times the thread entered the critical section */
    printf("Thread %d Entered Critical Section %d times.\n", 
            pos_in_arr, num_times_entered_critical_section);
    fflush(stdout);

    return NULL;
}


/* The lock ony allows one thread at a time to pass this function */
void lock(int i){
    /* Block while get a number for this thread */
    entering[i] = TRUE;
    number[i] = 1 + max_num();
    entering[i] = FALSE;

    int j;
    for(j = 0; j < num_threads; j++){

        // Wait until thread j receives its number:
        while(entering[j]){ 
            sched_yield();
            // yield to a different thread
        }

        // Wait until all threads with smaller numbers or with the same
        // number, but with higher priority, finish their work:
        while(number[j] != 0 && (number[j] < number[i] || (
                        number[j] == number[i] && j < i))){
            sched_yield();
            // yield to a different thread
        }
    }
}


/* reset the number for this thread */
void unlock(int i){
    number[i] = 0;
}


/* Crash the program if the lock is broken */
void critical_section(){
    assert(in_cs == 0);
    in_cs++;
    assert(in_cs == 1);
    in_cs++;
    assert(in_cs == 2);
    in_cs++;
    assert(in_cs == 3);
    in_cs = 0;
}


/**
 * Finds the max_value in the numbers array
 */
int max_num(){
    int max_val = 0;
    int pos;
    for(pos = 0; pos < num_threads; pos++){
        if(number[pos] > max_val)
            max_val = number[pos];
    }
    return max_val;
}
