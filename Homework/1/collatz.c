#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>


int main(int argc, char* argv[]){
    if(argc != 2){
        printf("Usage: collatz <starting_number>\n"
               "where starting number is a positive integer\n");
        return 1;
    }
    int n = atoi(argv[1]);
    if(n < 1){
        printf("starting_number should be positive\n");
            return 2;
    }

    // pid of child is returned in parent
    // 0 is return in the child
    pid_t pid = fork();
    if(pid == -1){
        perror("Fork Error:");
        exit(3);
    }
    else if(pid == 0){
        printf("%d ", n);
        while(n != 1){
            // if n is even
            // n = n/2
            if(n % 2 == 0){
                n = n/2;
            }
            // else n = 3 * n + 1
            else{
                n = 3*n + 1;
            }
            printf("%d ", n);
        }
        printf("\n");
        int fflush_status = fflush(stdout);
        if(fflush_status != 0){
            perror("fflush error:");
            exit(5);
        }
    }
    else{
        int* status = 0;
        int err_status = wait(status);
        if(err_status == -1){
            perror("Wait Error:");
            exit(4);
        }
    }
    return 0;
}
